---
title: Legal notice
date: 2020-11-14
---

The information in this weblog is provided “AS IS” with no warranties, and confers no rights.

This weblog does not represent the thoughts, intentions, plans or strategies of my employer, whoever it is at this moment. It is solely my opinion.

<h2>Cookies</h2>

We use no cookies.